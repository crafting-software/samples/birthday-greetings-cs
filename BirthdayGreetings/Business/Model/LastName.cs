namespace BirthdayGreetings.Business.Model;

public sealed record LastName(string Value)
{
    public static LastName Empty()
    {
        return new LastName(String.Empty);
    }

    public static LastName Of(string value)
    {
        return new LastName(value);
    }
}
