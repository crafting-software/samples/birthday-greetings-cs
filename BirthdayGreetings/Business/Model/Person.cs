namespace BirthdayGreetings.Business.Model;

public sealed record Person(
        FullName FullName,
        Birthdate Birthdate,
        Address Address)
{
    public static Person Nobody()
    {
        return Named(FirstName.Empty());
    }

    public static Person Named(FirstName firstname)
    {
        return new Person(
            FullName.On(firstname),
            Birthdate.Undefined(),
            Address.Undefined());
    }

    public Person With(Birthdate value)
    {
        return this with { Birthdate = value };
    }

    public Person With(FirstName value)
    {
        return this with { FullName = FullName.With(value) };
    }

    public Person With(LastName value)
    {
        return this with { FullName = FullName.With(value) };
    }

    public Person With(Address value)
    {
        return this with { Address = value };
    }

    public bool IsBirthday(DateOnly today)
    {
        return Birthdate.IsBirthday(today);
    }
}
