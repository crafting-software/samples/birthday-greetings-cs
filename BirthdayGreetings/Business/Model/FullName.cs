namespace BirthdayGreetings.Business.Model;


public sealed record FullName(FirstName FirstName, LastName LastName)
{
    public static FullName Undefined()
    {
        return new FullName(FirstName.Empty(), LastName.Empty());
    }

    public static FullName On(FirstName firstName, LastName lastName)
    {
        return new FullName(firstName, lastName);
    }

    public static FullName On(FirstName firstName)
    {
        return new FullName(firstName, LastName.Empty());
    }

    public static FullName On(LastName lastName)
    {
        return new FullName(FirstName.Empty(), lastName);
    }

    public FullName With(FirstName value)
    {
        return this with { FirstName = value };
    }

    public FullName With(LastName value)
    {
        return this with { LastName = value };
    }
}
