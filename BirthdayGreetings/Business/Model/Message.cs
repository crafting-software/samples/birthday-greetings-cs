namespace BirthdayGreetings.Business.Model;

public record Message
{
    public sealed record GreetingMessage(Person Recipient) : Message();

    public static Message Greeting(Person recipient)
    {
        return new GreetingMessage(recipient);
    }

    private Message() { }
    // Prevent derived cases from being defined elsewhere
}
