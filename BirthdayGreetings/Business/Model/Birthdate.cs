namespace BirthdayGreetings.Business.Model;

using BirthdayGreetings.Utils;

public sealed record Birthdate(DateOnly date, bool defined)
{
    public static Birthdate Undefined()
    {
        return new Birthdate(DateOnly.MinValue, false);
    }

    public static Birthdate From(string value)
    {
        return new Birthdate(DateHelper.FromIso(value), true);
    }

    public static Birthdate From(DateOnly value)
    {
        return new Birthdate(value, true);
    }

    public bool IsBirthday(DateOnly today)
    {
        if (!defined)
        {
            return false;
        }
        if (today.Year < date.Year)
        {
            return false;
        }
        if (today.Month != date.Month)
        {
            return false;
        }
        if (today.Day == date.Day)
        {
            return true;
        }
        if (!DateTime.IsLeapYear(today.Year) && today.Month == 2)
        {
            if (date.Day == 29 && today.Day == 28)
            {
                return true;
            }
        }
        return false;
    }
}
