namespace BirthdayGreetings.Business.Model;

public sealed record FirstName(string Value)
{
    public static FirstName Empty()
    {
        return new FirstName(String.Empty);
    }

    public static FirstName Of(string value)
    {
        return new FirstName(value);
    }
}
