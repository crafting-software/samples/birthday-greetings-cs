namespace BirthdayGreetings.Business.Model;

using System.Net.Mail;

public record Address
{
    public sealed record UndefinedAddress() : Address();
    public sealed record EmailAddress(MailAddress value) : Address();

    public static Address Undefined()
    {
        return new UndefinedAddress();
    }

    public static Address Email(string value)
    {
        return new EmailAddress(new MailAddress(value));
    }

    private Address() { }
    // Prevent derived cases from being defined elsewhere
}
