namespace BirthdayGreetings.Business.Usecases;

public interface IPersonProvider
{
    public List<Model.Person> ListAll();
}
