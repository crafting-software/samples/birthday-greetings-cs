namespace BirthdayGreetings.Business.Usecases;

public interface IMessageDispatcher
{
    void Dispatch(Model.Message message);
}
