namespace BirthdayGreetings.Business.Usecases;

public sealed class BirthdayGreeting
{
    private IPersonProvider peoples;
    private IDateProvider date;
    private IMessageDispatcher dispatcher;

    public BirthdayGreeting(IPersonProvider peoples, IDateProvider date, IMessageDispatcher dispatcher)
    {
        this.peoples = peoples;
        this.date = date;
        this.dispatcher = dispatcher;
    }

    public void ProcessForToday()
    {
        ProcessFor(date.Today());
    }

    public void ProcessFor(DateOnly today)
    {
        foreach (var person in this.peoples.ListAll())
        {
            if (person.IsBirthday(today))
            {
                this.dispatcher.Dispatch(
                    Model.Message.Greeting(person));
            }
        }
    }
}
