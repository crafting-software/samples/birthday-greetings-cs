namespace BirthdayGreetings.Business.Usecases;

public interface IDateProvider
{
    public DateOnly Today();
}
