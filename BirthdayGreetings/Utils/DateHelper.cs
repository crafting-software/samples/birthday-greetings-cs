namespace BirthdayGreetings.Utils;

public class DateHelper
{
    public static DateOnly FromIso(string value)
    {
        return DateOnly.ParseExact(
            value,
            new string[] { "yyyy-MM-dd" },
            System.Globalization.CultureInfo.InvariantCulture);
    }
}
