namespace BirthdayGreetingsTest;

using BirthdayGreetings.Business.Model;
using System.Net.Mail;
using FsCheck;

public class AddressUnitTest
{
    [Test, Sequential]
    public void AddressEmail_should_accept_only_valid_mail_address(
        [Values(
            "someone@somwhere.com",
            "someone+sub@somwhere.com")] string value)
    {
        Assert.That(Address.Email(value), Is.Not.Null);
    }

    [Test, Sequential]
    public void AddressEmail_should_reject_invalid_mail_address(
        [Values(
            "someone@",
            "@somwhere.com",
            "someone_somwhere.com")] string value)
    {
        Assert.That(
            () => Address.Email(value),
            Throws.TypeOf<FormatException>());
    }

    [Test]
    public void EmailAddress_should_1()
    {
        Prop.ForAll(
            (MailAddress value) =>
                Address.Email(value.Address) is not null).
            QuickCheckThrowOnFailure();
    }
}
