namespace BirthdayGreetingsTest;

using BirthdayGreetings.Business.Model;
using BirthdayGreetings.Utils;
using FsCheck;


public class BirthdateUnitTest
{
    [Test]
    public void IsBirthday_should_true_when_same_month_and_day_of_month()
    {
        Assert.That(
            Birthdate.From("2000-01-01").IsBirthday(DateHelper.FromIso("2022-01-01")),
            Is.True);
    }

    [Test]
    public void IsBirthday_should_true_when_born_on_29_february_and_today_is_28_february_on_non_leap_years()
    {
        Assert.That(
            Birthdate.From("2004-02-29").IsBirthday(DateHelper.FromIso("2015-02-28")),
            Is.True);
    }

    [Test]
    public void IsBirthday_should_false_otherwise()
    {
        Assert.That(
            Birthdate.From("2000-01-01").IsBirthday(DateHelper.FromIso("2018-07-01")),
            Is.False);
        Assert.That(
            Birthdate.From("2004-02-29").IsBirthday(DateHelper.FromIso("2016-02-28")),
            Is.False);
        Assert.That(
            Birthdate.From("2004-02-29").IsBirthday(DateHelper.FromIso("2005-02-28")),
            Is.True);
    }

    [Test]
    public void IsBirthday_should_true_when_same_month_and_day_of_month__property()
    {
        Prop.ForAll(
            (DateTime date, int delta) =>
            {
                var birthday = DateOnly.FromDateTime(date);
                var today = birthday.AddYears(delta);
                return (Birthdate.From(birthday).IsBirthday(today) is true)
                .When(delta >= 0);
            }).
            QuickCheckThrowOnFailure();
    }

    [Test]
    public void IsBirthday_should_manage_29_february_case__property()
    {
        Prop.ForAll(
            (int delta) =>
            {
                var birthday = new DateOnly(2004, 02, 29);
                var today = new DateOnly(2004, 2, 28).AddYears(delta);
                var isBirthday = !DateTime.IsLeapYear(today.Year);
                return (Birthdate.From(birthday).IsBirthday(today) == isBirthday)
                .When(delta > 0);
            }).
            QuickCheckThrowOnFailure();
    }

    [Test]
    public void IsBirthday_should_false_when_same_month_and_day_of_month_but_year_before_birth__property()
    {
        Prop.ForAll(
            (DateTime date, int delta) =>
            {
                var birthday = DateOnly.FromDateTime(date);
                var today = birthday.AddYears(-delta);
                return (Birthdate.From(birthday).IsBirthday(today) is false)
                .When(delta > 0);
            }).
            QuickCheckThrowOnFailure();
    }
}
