namespace BirthdayGreetingsTest;

using BirthdayGreetings.Business.Model;
using BirthdayGreetings.Business.Usecases;
using BirthdayGreetings.Utils;


[TestFixture, FixtureLifeCycle(LifeCycle.InstancePerTestCase)]
public class BirthdayGreetingsUnitTest
{
    private FixedDateProvider date = new FixedDateProvider();
    private MemoryPersonProvider peoples = new MemoryPersonProvider();
    private BirthdayNotificationCollector collector = new BirthdayNotificationCollector();

    [Test]
    public void BirthdayGreeting_With_Birthday_To_Wish()
    {
        GivenTodayIs("2022-03-01");
        AndPerson("Alexa", "2000-03-01");
        AndPerson("Alice", "2004-08-12");
        AndPerson("Isabelle", "2004-03-01");
        ThenThePeopleWeWishForTheirBirthdayShouldBe("Alexa, Isabelle");
    }

    [Test]
    public void BirthdayGreeting_With_No_Birthday_To_Wish()
    {
        GivenTodayIs("2022-12-05");
        AndPerson("Alexa", "2000-03-01");
        AndPerson("Alice", "2004-08-12");
        AndPerson("Isabelle", "2004-03-01");
        ThenWeShouldNotWishTheBirthdayToAnyone();
    }

    public void BirthdayGreeting_With_29_February_Rule()
    {
        GivenTodayIs("2015-02-28");
        AndPerson("Peter", "2000-01-01");
        AndPerson("Yann", "2004-02-29");
        AndPerson("Arthur", "2004-02-28");
        ThenThePeopleWeWishForTheirBirthdayShouldBe("Yann, Arthur");
    }

    private void GivenTodayIs(string today)
    {
        date.SetTodayTo(DateHelper.FromIso(today));
    }

    private void AndPerson(string name, string birthdate)
    {
        peoples.Add(
            Person.Named(FirstName.Of(name))
                  .With(Birthdate.From(birthdate)));
    }

    private void ThenThePeopleWeWishForTheirBirthdayShouldBe(string expected)
    {
        Assert.That(PersonsToWishBirthday(), Is.EqualTo(expected));
    }

    private void ThenWeShouldNotWishTheBirthdayToAnyone()
    {
        Assert.That(PersonsToWishBirthday(), Is.EqualTo(""));
    }

    private String PersonsToWishBirthday()
    {
        new BirthdayGreeting(peoples, date, collector).ProcessForToday();
        var result = collector.notified.ConvertAll(p => p.FullName.FirstName.Value);
        result.Sort();
        return string.Join(", ", result);
    }
}

public class MemoryPersonProvider : IPersonProvider
{
    private List<Person> peoples = new List<Person>();

    public void Add(Person person)
    {
        this.peoples.Add(person);
    }

    public List<Person> ListAll()
    {
        return peoples;
    }
}


public class FixedDateProvider : IDateProvider
{
    private DateOnly today;

    public void SetTodayTo(DateOnly today)
    {
        this.today = today;
    }

    public DateOnly Today()
    {
        return today;
    }
}

public class BirthdayNotificationCollector : IMessageDispatcher
{
    public List<Person> notified = new List<Person>();

    public void Dispatch(Message message)
    {
        switch (message)
        {
            case Message.GreetingMessage greeting:
                Add(greeting.Recipient);
                break;
        }
    }

    public void Add(Person person)
    {
        notified.Add(person);
    }
}
